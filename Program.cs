﻿using System;

namespace Oef_6_1_8
{
    class Program
    {


        static void Main(string[] args)
        {
            // Oef 6.1.8
            Console.Title = "MASTERMIND";

            int[] arrayGok = { 0, 0, 0, 0 };
            int[] opgave = { 0, 0, 0, 0 };
            bool gameWon = false;
            Random RandGen = new Random();
            int juistePaats = 0;
            int foutePlaats = 0;
            int teller = 0;
            // Zet volgende als false om de opgave te verbergen
            bool cheating = false;
            // Als je enkel met de kleuren wil spelen zet je deze op true
            // Anders worden de cijfers en de kleuren getoond.
            bool colorsOnly = false;

            //  2 dimensonele array met 4 ints voor de gokken en 2 ints voor juiste en foute plaats.
            int[,] gokken = { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }};
            // genereer opgave
            for (int i = 0; i < 4; i++)
            {
                opgave[i] = RandGen.Next(1, 7);
            }
            // Printing the header
            if (cheating)
            {
                PrintTitling(opgave[0].ToString() + opgave[1] + opgave[2] + opgave[3], colorsOnly);
            }
            else
            {
                PrintTitling("", colorsOnly);
            }
            // Game started
            while (gameWon == false && teller < 12)
            {
                string padding;
                // ervoor zorgen dat de ingegeven gok mooi gealigneerd blijft  bij de 10 de en hoger gok
                if (teller < 9)
                {
                    padding = "  ";
                }
                else
                {
                    padding = " ";
                }
                Console.Write("Gok {0} van 12:{1}", teller + 1, padding);
                arrayGok = GetPlayerGuess(colorsOnly);

                bool[] geradenPionnen = { false, false, false, false };
                juistePaats = 0;
                foutePlaats = 0;
                for (int c = 0; c < 4; c++)
                {
                    gokken[teller, c] = arrayGok[c];
                    if (arrayGok[c] == opgave[c])
                    {
                        juistePaats++;
                        geradenPionnen[c] = true;
                    }
                }

                for (int c = 0; c < 4; c++)
                {
                    if (arrayGok[c] != opgave[c])
                    {
                        for (int e = 0; e < 4; e++)
                        {
                            if (arrayGok[c] == opgave[e] && !geradenPionnen[e])
                            {
                                foutePlaats++;
                                // Toevoegen aan geraden pionnen zodat deze niet herbruikt worden
                                geradenPionnen[e] = true;
                                break;
                            }
                        }
                    }
                }
                gokken[teller, 4] = juistePaats;
                gokken[teller, 5] = foutePlaats;
                teller++;
                if (juistePaats == 4)
                    gameWon = true;
                // Printing naar het scherm
                Console.Clear();
                if (cheating)
                {
                    PrintTitling(opgave[0].ToString() + opgave[1] + opgave[2] + opgave[3], colorsOnly);
                }
                else
                {
                    PrintTitling("", colorsOnly);
                }
                for (int i = 0; i < teller; i++)
                {
                    int[] arrayToPrint = { gokken[i, 0], gokken[i, 1], gokken[i, 2], gokken[i, 3], gokken[i, 4], gokken[i, 5] };
                    PrintGok(arrayToPrint, colorsOnly);
                }
            }
            if (gameWon == true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\n\nProficiat! Code gekraakt in {0} keer.", teller);
            }
            else
            {
                Console.Write("\n\nJammer, je kon de code niet kraken. \nDe opgave was: \n");
                int[] printOpgave = { opgave[0], opgave[1], opgave[2], opgave[3], 0, 0 };
                PrintGok(printOpgave, colorsOnly);
            }
            Console.ReadKey();
        }

        static int[] GetPlayerGuess(bool colorsOnly)
        {
            int[] guess = { 1, 2, 3, 4 };
            int arrayPointer = 0;

            while (arrayPointer < 5)
            {
                string cki = Console.ReadKey(true).Key.ToString();
                bool correctInput = false;
                int value = 0;

                switch (cki)
                {
                    case "D1":
                    case "NumPad1":
                        Console.ForegroundColor = ConsoleColor.Red;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(1);
                        }
                        value = 1;
                        correctInput = true;
                        break;
                    case "D2":
                    case "NumPad2":
                        Console.ForegroundColor = ConsoleColor.Blue;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(2);
                        }
                        value = 2;
                        correctInput = true;
                        break;
                    case "D3":
                    case "NumPad3":
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(3);
                        }
                        value = 3;
                        correctInput = true;
                        break;
                    case "D4":
                    case "NumPad4":
                        Console.ForegroundColor = ConsoleColor.Green;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(4);
                        }
                        value = 4;
                        correctInput = true;
                        break;
                    case "D5":
                    case "NumPad5":
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(5);
                        }
                        value = 5;
                        correctInput = true;
                        break;
                    case "D6":
                    case "NumPad6":
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(6);
                        }
                        value = 6;
                        correctInput = true;
                        break;
                    case "Escape":
                        Environment.Exit(0);
                        break;
                    case "Backspace":
                        if (arrayPointer > 0)
                        {
                            if (arrayPointer < 3)
                            {
                                // Console.Write("\u001b[{n}C\u001b[{n}C  \u001b[{n}C\u001b[{n}C");
                                Console.Write("\u001b[2D  \u001b[2D");
                                arrayPointer--;
                            }
                            else
                            {
                                Console.Write("\u001b[6D      \u001b[6D");
                                arrayPointer--;
                            }
                        }
                        break;
                    default:
                        if (arrayPointer == 4)
                        {
                            correctInput = true;
                        }
                        break;
                }
                if (correctInput)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    if (arrayPointer < 3)
                    {
                        Console.Write("|");
                    }
                    else
                    {
                        Console.Write("  Ok?");
                    }
                    if (arrayPointer < 4)
                    {
                        guess[arrayPointer] = value;
                    }
                    arrayPointer++;
                }
            }
            return guess;
        }

        static void PrintGok(int[] arrayToPrint, bool colorsOnly)
        {
            Console.Write("               ");
            for (int i = 0; i < 4; i++)
            {
                switch (arrayToPrint[i])
                {
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Red;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(1);
                        }
                        break;
                    case 2:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(2);
                        }
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(3);
                        }
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.Green;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(4);
                        }
                        break;
                    case 5:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(5);
                        }
                        break;
                    case 6:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        if (colorsOnly)
                        {
                            Console.Write("▌");
                        }
                        else
                        {
                            Console.Write(6);
                        }
                        break;
                }
                Console.ForegroundColor = ConsoleColor.White;
                if (i < 3)
                    Console.Write("|");
            }
            Console.Write("   ");
            Console.ForegroundColor = ConsoleColor.White;
            // Write ▌ naaaar het scherm in het wit voor juiste  plaats
            for (int i = 0; i < arrayToPrint[4]; i++)
            {
                Console.Write("▌");
            }
            Console.ForegroundColor = ConsoleColor.DarkGray;
            // Write ▌ naaaar het scherm in het grijs voor foute plaats
            for (int i = 0; i < arrayToPrint[5]; i++)
            {
                Console.Write("▌");
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("\n");
        }

        static void PrintTitling(string cheat, bool colorsOnly)
        {

            // Console.WriteLine("\u001b[31mHello World!\u001b[0m");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("██████████████████████████████████████████████████████████████");
            Console.Write("███████ ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Welkom bij MASTERMIND - Game of the year 1972! ");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("███████");
            Console.WriteLine("██████████████████████████████████████████████████████████████");
            if (colorsOnly)
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("  " + 1 + "=  ▌");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("  " + 2 + "=  ▌");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("  " + 3 + "=  ▌\n");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("  " + 4 + "=  ▌");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("  " + 5 + "=  ▌");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("  " + 6 + "=  ▌");
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("\n\n  ▌");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine(" betekent pionnen op de juiste plaats.");
            Console.WriteLine("  ▌ betekent pionnen op de foute plaats.");
            Console.WriteLine("  Je kan altijd op Esc duwen om af te sluiten.");
            Console.WriteLine("  Met backspace kan je eventueel corrigeren.");

            if (cheat != "")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Valsspeler!!! ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(cheat);
            }
            Console.WriteLine("\n");

            Console.ForegroundColor = ConsoleColor.White;

        }

    }
}

